package ThreadDemo;

/**
 * @author David "Sky" Leister
 */
public class ThreadDemo {
    
    public static void main(String[] args) {
        
        Arbeiter zaehler = new Arbeiter();
        
        Thread erster = new Thread(zaehler);
        Thread zweiter = new Thread(zaehler);
        
        erster.setName("Erster");
        zweiter.setName("Zweiter");
        
        System.out.println("Starte...");
        erster.start();
        zweiter.start();
        System.out.println("...fertig!");
        
    }

}
